# Created by : Mustafa Sadriwala
# Assignment : Web Development basics

In this assignment we were supposed to create a git repository in which we were supposed to create files corresponding to components used in web development.
Also to write different tools available to develop those components along with some brief details and explaination for them.

My repository includes files for: 

1.  Frontend
2.  Backend
3.  Database
4.  Domain / Hosting
5.  Cloud Service Provider
6.  Continous Integration / Continous Development
7.  Various Tools
8.  Types of status codes
9.  MVC supported and MVC not supported frameworks
10. Technology stacks